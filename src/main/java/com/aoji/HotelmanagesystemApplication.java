package com.aoji;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HotelmanagesystemApplication {

    public static void main(String[] args) {
        SpringApplication.run(HotelmanagesystemApplication.class, args);
    }

}
