package com.aoji.controllor;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.thymeleaf.util.StringUtils;
import sun.security.pkcs11.Secmod;

import javax.servlet.http.HttpSession;

@Controller
public class LonginController {

    @RequestMapping("/user/login")
    public String login(@RequestParam("username") String username, @RequestParam("password") String paasword,
                        Model model, HttpSession session){
        if(!StringUtils.isEmpty(username)&&"123456".equals(paasword)){
            session.setAttribute("loginUser",username);
            return "redirect:/main.html";
        }else{
            model.addAttribute("msg","用户名或者密码错误！");
            return "index";
        }

    }
}
