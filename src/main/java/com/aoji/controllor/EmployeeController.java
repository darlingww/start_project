package com.aoji.controllor;

import com.aoji.Dao.DepartmentDao;
import com.aoji.Dao.EmployeeDao;
import com.aoji.pojo.Department;
import com.aoji.pojo.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@Controller
public class EmployeeController {
    @Autowired
    EmployeeDao employeeDao;
    @Autowired
    DepartmentDao departmentDao;
    @RequestMapping("/emps")
    public String list(Model model){
        Collection<Employee> employees=employeeDao.getAll();
        model.addAttribute("emps",employees);
        return "emp/list";
    }
    @GetMapping("/emp")
    public String toAddpage(Model model){
        //查处所有部门的信息
       Collection<Department> departments= departmentDao.getDepartments();
       model.addAttribute("departments",departments);
        return "add2";
    }
    @PostMapping("/emp")
    public String addEmp(Employee employee){
      //添加的操作
        System.out.println("save=>"+employee);
      employeeDao.save(employee);
        return "redirect:/emps";
    }
    //去员工的修改页面
    @GetMapping ("/emp/{id}")
    public String toUpdateEmp(@PathVariable("id") Integer id, Model model){
        //查出原来的数据
        Employee employee=employeeDao.getEmployeeById(id);
        model.addAttribute("emp",employee);
        System.out.println("出来");
        return "emp/update";
    }
}
