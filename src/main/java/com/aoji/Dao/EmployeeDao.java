package com.aoji.Dao;

import com.aoji.pojo.Department;
import com.aoji.pojo.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

//员工Dao
@Repository//用于持久层，把对象就交给Spring管理
public class EmployeeDao {
    //模拟数据库中的数据
    private static Map<Integer, Employee> employees=null;
    //员工有所属的部门
    @Autowired
    private DepartmentDao departmentDao;
    static {
        employees=new HashMap<Integer, Employee>();//创建一个部门表
        employees.put(101,new Employee(1001,"AA","A123456@qq.com",1,new Department(101,"教学部")));
        employees.put(102,new Employee(1002,"BB","B123456@qq.com",1,new Department(102,"市场部")));
        employees.put(103,new Employee(1003,"CC","C123456@qq.com",1,new Department(103,"教研部")));
        employees.put(104,new Employee(1004,"DD","D123456@qq.com",1,new Department(104,"运营部")));
        employees.put(105,new Employee(1005,"EE","E123456@qq.com",1,new Department(105,"后勤部")));
    }
    //增加
    private static Integer initId=1006;
    public void save(Employee employee){
        //主键自增
        if(employee.getId()==null){
            employee.setId(initId++);
        }
        //给员工设置部门(部门关联外键)
        employee.setDepartment(departmentDao.getDepartmentById(employee.getDepartment().getId()));
        //  往数据库中添加
        employees.put(employee.getId(),employee);
    }

    //查询员工信息
    public Collection<Employee> getAll(){
        return employees.values();
    }
    //通过Id查询员工信息
    public Employee getEmployeeById(Integer id){
        return employees.get(id);
    }
    //通过Id删除员工
    public void delete(Integer id){
        employees.remove(id);
    }
}
